# v2ray
# [一键搭建V2Ray服务器教程2023]使用 Vultr VPS 自建V2Ray节点机场及客户端配置多用户实现科学上网

本系列教程适合新手小白，当然老鸟也可以看看，也许会有你还不知道的黑科技。[一灯不是和尚](https://iyideng.fun/)手把手教你搭建SS/SSR/V2Ray/Trojan节点服务器并实现科学上网，这可能是史上最全、最简单、最适合小白萌新的一键搭建V2Ray服务器教程。本教程内容包括购买域名和VPS，远程连接并管理VPS和V2Ray一键安装脚本最新版以及多用户配置的方法，并开启BBR加速以及V2Ray客户端配置教程。



## **1、注册域名和购买非中国大陆地区的VPS**

### **（1）注册域名**

如果你使用“Vmess+TLS+Web+CDN”模式的话，就需要用域名伪装成网站，否则不需要域名。注册域名有两种途径：注册国外免费域名和付费购买域名。

**1）注册免费域名**

注册免费域名请参考文章 [最新的国外免费顶级域名网站Freenom注册免费域名教程与Cloudflare托管解析的方法](https://iyideng.fun/welfare/freenom-free-domain-register.html)

**2）购买付费域名**

购买付费域名推荐 [NameSilo](https://ad.bgfw.cc/NameSilo)(推荐) 或 [Namecheap](https://ad.bgfw.cc/Namecheap)，大部分域名一般不到4美元/第1年，像 .xray 和 .top 这样顶级域名还不到1美元/年，非常划算，第2年重新注册一个新的就可以了。

如果你打算在 NameSilo 注册域名的话，请参考文章 [NameSilo – 美国知名域名注册商 | 仅年付0.99/1.99美元 | 域名购买与账户注册图文教程](https://iyideng.fun/note/namesilo-domain-registrar.html)

**3）解析域名**

注册好域名之后，请把域名解析到你要搭建V2Ray服务器所用VPS对应的IP地址，一般5分钟之内就可以生效，最迟72小时生效。我一直推荐大家使用 Cloudflare 管理域名，解析速度快，基本秒生效，非常方便快捷，而且 Cloudflare 的免费 CDN 很好用，而且搭建自用SS/SSR/V2Ray/Trojan机场经常会用到。

关于互联网域名注册、购买与添加DNS域名解析记录的详细操作教程，请参考文章 [互联网域名注册、服务购买与添加域名解析记录及更改DNS服务器的详细图文教程](https://iyideng.fun/note/domain-name-registration-buy-resolution-and-change-dns.html)

### **（2）购买非中国大陆地区的VPS**

**1）为什么要购买非中国大陆地区的VPS？**

因为中国大陆经营的正规VPS提供商都在国家正规备案的，不允许你用VPS搭建V2Ray节点服务器，一旦发现你违规使用会停掉你的VPS，还可能不退款。另外，即使你可以搭建科学上网服务器，而且没有被发现，那么也可能被监控，并有隐私泄露的风险，因为像国内大厂的云产品上面都有监控代码，你未必能清理干净。鉴于以上原因，我建议你选择非天朝公司且不在大陆备案经营的VPS提供商。

**2）使用香港或者澳门公司的VPS怎么样？**

当然是可以的，但是他们都很贵，而且仍是天朝下辖地区，还是不碰为好。我推荐你选择美国、台湾、日本、新加坡、韩国等地区机房的VPS，甚至欧洲公司的产品，毕竟中国台湾还没有被统一，天朝直接管不了。

通过以上分析和介绍，我相信你也不愿意选择阿里云、腾讯云、百度云和华为云等这样的国内大公司的VPS，最好选择美国西海岸、日本东京或新加坡机房的VPS。

**3）用什么VPS搭建V2Ray服务器比较好？**

国外VPS哪家好？一键搭建V2Ray服务器推荐你使用 **[Vultr](https://iyideng.fun/special/vps/latest-vultr-vps-registration-purchase-and-installation-system-tutorial.html)**（**推荐**）、**[搬瓦工(BandwagonHOST)](https://iyideng.fun/special/vps/bandwagonhost-vps-all.html)** 或 **[Hostwinds](https://ad.bgfw.cc/Hostwinds)** 等大公司的VPS。其中，Vultr在全球拥有30个数据中心，虽然在不同国家和地区的机房数据中心对中国大陆不同地域网络的访问速度和延迟有些差别，但你只需要测试好最适合自己当地网络环境的机房位置即可，因为你可以方便且无限制地免费更换IP，直至找到最适合你的那个数据中心。**毋庸置疑，在优质国外VPS服务商中，Vultr是性价比最高、最值得推荐的一家。**另外，BandwagonHOST(搬瓦工)在中国大陆的知名度非常高，它的速度和稳定性都很不错，尤其是 CN2 GIA 线路的套餐，但价格非常贵，是同类产品中相对较贵的，性价比一般，而且现在换IP也非常贵。最后，鉴于Hostwinds在国外口碑非常好，服务器安全稳定，还支持免费更换IP，网站不仅支持中文操作界面，而且有中文客服实时在线，所以也是值得一试的。

如果你是老鸟的话，你应该懂得任何一家VPS都有值得推荐的优势。如果你追求速度和稳定性的话，我推荐您使用有中国电信 CN2 GIA、中国联通 CUVIP(AS9929) 或移动CMI，甚至日本软银等高端线路的VPS。其中，由于中国香港、中国台湾、日本和韩国的数据中心更靠近中国大陆，网络延迟相对更低，连接响应速度会更快，但峰值网络带宽并不一定高。虽然这些国家和地区的物理优势明显，但价格也是比较贵，尤其 CN2 GIA 和CU2(AS9929) 线路非常昂贵，不太适合普通用户，所以我们一般选择较多选择美国或欧洲机房的特殊优化或高端线路，性价比相对较高。**如果你追求超高性价比的话，我相信Vultr肯定是你最好的选择。**

关于 Vultr 的账户注册、套餐购买和VPS服务器系统安装与远程管理的详细使用教程，请参考 [最新Vultr账户注册、VPS套餐购买与服务器系统安装以及SSH远程管理的详细图文教程](https://iyideng.fun/special/vps/latest-vultr-vps-registration-purchase-and-installation-system-tutorial.html)，鉴于图文教程已经非常详尽，我这里就不再赘述，我后面的图文教程均以 Vultr VPS 为例进行演示。

## **2、远程连接并管理 Vultr VPS 服务器**

关于 Xshell/Putty 远程连接并管理 Vultr VPS 服务器的详细使用教程，请参考 [最新Vultr账户注册、VPS套餐购买与服务器系统安装以及SSH远程管理的详细图文教程](https://iyideng.fun/special/vps/latest-vultr-vps-registration-purchase-and-installation-system-tutorial.html#5SSH_lian_jie_yuan_cheng_Vultr_VPS)，鉴于图文教程已经非常详尽，我这里就不再赘述。

当完成 Vultr 账户注册、套餐购买并成功连接到远程 VPS 之后，我们就可以继续下面的步骤了。

## **3、一键搭建V2Ray服务器教程**

温馨提示：由于V2Ray对时间要求比较严格，要求误差必须在90秒以内，否则会导致安装失败。另外，时间误差跟服务器系统的时区没有直接关系，任意时区都可以，你也可以把服务器设置为跟你本地计算机处于同一时区。

我这里选择 Vultr VPS 服务器位置在 Seattle(美国西雅图)，安装 CentOS 7 X64 系统，使用 Xshell 远程登陆 Vultr VPS 进行操作，然后我们就可以使用Vultr搭建V2Ray服务器了。具体操作步骤如下：

### **（1）安装时间同步组件ntp**

一般情况下，我们不需要安装此组件，在需要进行服务器时间同步校对时启用，使服务器时间保持跟全球网络同步。

```
yum -y install ntp #CentOS/Fedora
apt install ntp -y #Debian/Ubuntu
```

### **（2）执行V2Ray一键安装脚本**

虽然各路技术大神编写的V2Ray一键安装脚本有很多种，但是代码技术水平和使用难易程度却参差不齐。经过慎重测试，并结合长期使用经验，这里我选中并推荐以下3款一键搭建脚本，分别是233boy大神的“V2Ray 一键安装脚本 & 管理脚本 By 233v2.com”和Jrohy大神的“multi-v2ray 多用户管理脚本”，还有sprov065大神的“v2-ui 多协议多用户管理面板”。

其中，233boy的脚本是最出名的一款，在 CentOS/Fedora 系统使用时必须手动放行所用的服务器端口，否则无法成功科学上网，我建议使用 Debian 9 及以上系统搭建；sprov065大神对于新手，甚至是有一定经验的用户都是有门槛的，不推荐小白萌新使用，因为你很难搞定。我最推荐的是Jrohy大神的“multi-v2ray 多用户管理脚本”，简单快捷，而且功能强大，代码质量也很高，几乎很少出现问题（这可能跟使用习惯有关系，稳定顺手就好）。

关于这3款一键搭建V2Ray服务器脚本的详细介绍，请参考文章 [V2Ray一键脚本哪个好？全网最优秀的V2Ray一键搭建脚本推荐](https://iyideng.fun/black-technology/cgfw/best-onekey-build-v2ray-server-script.html)

#### **1）V2Ray 一键安装脚本 & 管理脚本 By 233v2.com**

**安装 Curl 依赖包**

```
yum update -y && yum install curl -y #CentOS/Fedora
apt-get update -y && apt-get install curl -y #Debian/Ubuntu
```

**执行V2Ray一键安装脚本命令**

```
bash <(curl -s -L https://git.io/v2ray.sh)
```

执行以上命令后，会进入一键脚本的安装/卸载选项菜单，如下图所示：

![img](images/QQ图片20200701173337.png)

我们输入数字“1”，进入安装配置向导，依次为“传输协议”-“端口”-“是否开启广告拦截”-“是否配置Shadowsocks”，此脚本默认使用TCP协议、随机端口号、关闭广告拦截、不配置Shadowsocks，我这里使用1688端口仅用于教程演示（个人癖好而已）。如果你是新手小白，那么全部使用默认值即可完成搭建，你也可以尝试使用mKCP协议，跟使用TCP协议一样简单。如下图所示：

![img](images/V2Ray-一键安装脚本-管理脚本-By-233v2.com-安装选项1.png)![img](images/V2Ray-一键安装脚本-管理脚本-By-233v2.com-安装选项2.png)![img](images/V2Ray-一键安装脚本-管理脚本-By-233v2.com-搭建完成后的服务器配置信息.png)

当我们使用V2Ray一键搭建脚本完成后，输入“v2ray url”可生成 vmess URL 链接，输入“v2ray qr”可生成二维码链接，将生成的 Vmess URL 添加到 V2Ray 客户端无法直接进行科学上网。这时候，你还需要手动放行服务器端口，相关命令如下：

```
查看已放行的端口：firewall-cmd --list-ports
放行服务器端口：firewall-cmd --zone=public --add-port=1688/tcp --permanent
关闭服务器端口：firewall-cmd --zone=public --remove-port=1688/tcp --permanent
重启防火墙：firewall-cmd --reload
```

**注意事项：**以上“1688”必须换成你自己的服务器端口号，“tcp”换成你正在使用的协议，放行服务器端口后，必须重启防火墙才能生效。

如果你想要修改配置信息，那么输入命令“v2ray”进行管理。

**V2Ray快速管理命令：**

v2ray info 查看 V2Ray 配置信息
v2ray config 修改 V2Ray 配置
v2ray link 生成 V2Ray 配置文件链接
v2ray infolink 生成 V2Ray 配置信息链接
v2ray qr 生成 V2Ray 配置二维码链接
v2ray ss 修改 Shadowsocks 配置
v2ray ssinfo 查看 Shadowsocks 配置信息
v2ray ssqr 生成 Shadowsocks 配置二维码链接
v2ray status 查看 V2Ray 运行状态
v2ray start 启动 V2Ray
v2ray stop 停止 V2Ray
v2ray restart 重启 V2Ray
v2ray log 查看 V2Ray 运行日志
v2ray update 更新 V2Ray
v2ray update.sh 更新 V2Ray 管理脚本
v2ray uninstall 卸载 V2Ray

由于233boy大佬的一键脚本过于出名而受到了重点照顾，如果你想要顺利开启“Vmess+WebSocket+TLS”模式，我建议你使用Jrohy大神的“multi-v2ray 多用户管理脚本”，更简单易用，而且成功率非常高，功能也更强大。

#### **2）multi-v2ray 多用户管理脚本（推荐）**

**温馨提醒：**现在，multi-v2ray 多用户管理脚本已增加支持一键搭建VLESS/VLESS+WS/VLESS+XTLS/Trojan/Xray服务器，功能更强大了。在这里，我推荐您使用 Debian 9/11 或 CentOS 7 系统搭建环境（推荐使用 Debian 11 系统），如果您使用其他系统环境搭建可能会遇到莫名其妙的问题，浪费时间和精力。经测试，使用 Debian 10 搭建，curl组件可能无法正常使用，导致一键脚本安装命令执行不成功，请认真斟酌！！！

在安装 Curl 依赖包过程中，如果出现以下提示，则表示Curl依赖包没有安装成功。**如下图所示：**

![img](images/修复并安装-Curl-依赖包.png)

此时，你执行V2Ray一键安装脚本命令，仍旧收到“**-bash: curl: command not found**”的提示。此时，请执行一下修复命令：apt –fix-broken install（注意是两个英文短杠“-”，而不是一个中文长杠“–”，文章一直显示不正常，我也没找到具体原因），出现“**Do you want to continue? [Y/n]**”的提示，输入“**y**”后，回车，稍等片刻，完成后再次执行安装Curl依赖包命令，就能成功安装Curl依赖包了。

**安装 Curl 依赖包**

```
yum update -y && yum install curl -y #CentOS/Fedora
apt-get update -y && apt-get install curl -y #Debian/Ubuntu
```

**执行一键安装脚本命令**

```
source <(curl -sL https://multi.netlify.app/v2ray.sh) --zh
```

回车执行上述代码，稍等片刻即可安装完成，期间不需要你任何操作。安装完成后，如下图所示：

![img](images/multi-v2ray-多用户管理脚本安装完成.png)

一键安装脚本默认使用kcp协议，并随机选择伪装方式（上图伪装成了微信视频(wechat-video)模式）。我们复制以上默认生成的vmess链接并添加到V2Ray客户端，既可成功实现科学上网。我这里不建议你使用这种默认的配置方式，我推荐你使用“Vmess+WS+TLS+Web+CDN”模式，既实现了伪装成互联网最普遍的HTTPS数据传输，并使用TLS高强度加密，又实现了对 VPS IP 地址的隐藏保护，更加安全稳定，但是速度会有所下降。

**温馨提醒：**如果使用“Vmess+WS+TLS+Web+CDN”模式，我们首先要使用 Cloudflare 进行域名解析，添加A记录时，请把小云彩点成灰色，即只使用DNS解析而不使用CDN，否则会造成绑定失败。“v2ray.mydomain.tk”全程只是作为操作演示域名使用，你具体操作时，请使用自己的域名。

开启V2Ray一键安装脚本“Vmess+WS+TLS+CDN”模式的 [A]、[B]、[C] 3个步骤：

**[A] 更改传输方式，启用WebSocket。**

我们输入“v2ray”即可进行V2Ray配置管理，然后依次选择“3.更改配置”-“5.更改传输方式”-“3.WebSocket”，然后输入你要伪装的域名，如“v2ray.mydomain.tk”，回车（不需要输入“https/https”，而且域名必须已经成功解析到你正在使用VPS的IP地址，我这里把伪装域名设置成要绑定的域名了，你也可以可以设置成其它的域名，但是这样成功率高）。如下图所示：

![img](images/multi-v2ray-多用户管理脚本-更改配置并启用WebSocket.png)

**[B] 启用TLS，并绑定域名。**

我们依次选择“3.更改配置”-“6.更改TLS设置”-“1.开启 TLS”-“1. Let’s Encrypt 生成证书(准备域名)”，然后在“请输入您绑定的域名：”处输入域名，比如“v2ray.mydomain.tk”，回车即可进入SSL证书安装过程，期间无需任何操作，直至完成。如下图所示：

![img](images/multi-v2ray-多用户管理脚本-开启TLS设置.png)

**[C] 开启CDN隐藏保护**

我们依次选择“3.更改配置”-“11.走CDN(需要域名)，然后在“当前组域名为: v2ray.mydomain.tk, 回车继续使用或者输入新的域名:”处，直接回车使用当前域名。接着依次选择“1.443”-“1. Let’s Encrypt 生成证书(准备域名)”，回车，开始再次安装SSL证书（其实，上步已经安装过了），直至最终完成，期间无需任何操作。如下图所示：

![img](images/multi-v2ray-多用户管理脚本-开启CDN设置-1.png)

当开启“Vmess+WebSocket+TLS+CDN”模式时，我建议使用443端口，伪装成互联网最常见的HTTPS协议的流量，抗封锁能力更强，更安全稳定。

当以上操作均成功完成后，去 Cloudflare 把域名解析的小云朵点亮即可。如下图所示：

![img](images/为Cloudflare域名解析的IP地址点亮小云朵，启用CDN.png)

你可以使用 “ping v2ray.mydomain.tk” 命令 或 站长工具IP查询进行检查 Cloudflare CDN 是否添加成功。一旦解析成功后，你就可以放肆地科学上网了（其实，不解析成功照样可以科学上网，只是启用 CDN 后更加安全稳定）。如下图所示：

![img](images/使用-“ping-v2ray.mydomain.tk”-命令检查-Cloudflare-CDN-是否添加成功.png)

使用[站长工具IP查询](http://ip.tool.chinaz.com/)会更加简单，如下图所示：

![img](images/使用站长工具IP查询是否为美国Cloudflare公司的CDN节点.png)

当以上方法查询，返回的IP地址是Cloudflare的美国 cloudflare节点时，就表示已经成功开启CDN。

**一键安装脚本升级命令(保留配置文件更新)**

```
source <(curl -sL https://multi.netlify.app/v2ray.sh) -k
```

**一键安装脚本卸载命令**

```
source <(curl -sL https://multi.netlify.app/v2ray.sh) --remove
```

**一键安装脚本的V2Ray管理命令行参数**

```
v2ray [-h|--help] [options]
    -h, --help           查看帮助
    -v, --version        查看版本号
    start                启动 V2Ray
    stop                 停止 V2Ray
    restart              重启 V2Ray
    status               查看 V2Ray 运行状态
    new                  重建新的v2ray json配置文件
    update               更新 V2Ray 到最新Release版本
    update.sh            更新 multi-v2ray 到最新版本
    add                  新增mkcp + 随机一种 (srtp|wechat-video|utp|dtls|wireguard) header伪装的端口(Group)
    add [wechat|utp|srtp|dtls|wireguard|socks|mtproto|ss]     新增一种协议的组，端口随机,如 v2ray add utp 为新增utp协议
    del                  删除端口组
    info                 查看配置
    port                 修改端口
    tls                  修改tls
    tfo                  修改tcpFastOpen
    stream               修改传输协议
    cdn                  走cdn
    stats                v2ray流量统计
    iptables             iptables流量统计
    clean                清理日志
    log                  查看日志
```

未开启BBR加速的情况下，在晚高峰看YouTube油管4K视频测速，如下图所示：

![img](images/晚高峰看YouTube油管4K视频测速.png)

#### **3）v2-ui 多协议多用户管理面板**

v2-ui 是一款非常优秀的支持多协议多用户的 V2Ray 面板，由原 sprov-ui 的作者sprov使用 Python 语言重新开发而成基于Web管理的可视化面板程序，但是不支持自动配置HTTPS，需要自备域名和SSL证书。

此脚本仅支持64位服务器系统，包括 CentOS 7+、Debian 8+、Ubuntu 16+，支持一键安装和升级。

**安装 Curl 依赖包**

```
yum update -y && yum install curl -y #CentOS/Fedora
apt-get update -y && apt-get install curl -y #Debian/Ubuntu
```

**执行一键安装脚本代码命令**

```
bash <(curl -Ls https://blog.sprov.xyz/v2-ui.sh)
```

或

```
bash <(curl -Ls https://raw.githubusercontent.com/mikewubox/v2-ui/master/install.sh)
```

此一键脚本真的只需要执行一次安装命令即可。安装成功后，如下图所示：

![img](images/v2-ui-多协议多用户管理面板-一键安装脚本安装完成.png)

此脚本最大的优点就是可视化的Web管理面板，但需要手动添加配置SSL证书，手动在后台配置网页面板根路径。可视化Web管理面板如下图所示：

![img](images/v2-ui-一键安装脚本-Web可视化面板.png)

虽然 v2-ui 是真正的傻瓜式一键安装脚本，但只不过有些操作需要在Web可视化面板和服务器端操作，还需要自己申请SSL证书并传到服务器，对新手不是很友好，甚至有一定基础的用户也未必能轻易操作成功，所以我还是建议小白萌新用户使用Jrohy大神的“multi-v2ray 多用户管理脚本”。

## **4、一键安装并开启BBR加速**

**（1）安装 wget 依赖包**

```
yum -y install wget #CentOS
apt-get install wget #Ubuntu/Debian
```

**（2）执行BBR加速一键安装脚本命令**

```
cd /usr/src && wget -N --no-check-certificate "https://raw.githubusercontent.com/chiakge/Linux-NetSpeed/master/tcp.sh" && chmod +x tcp.sh && ./tcp.sh
```

执行以上安装命令后，如下图所示：

![img](images/BBR加速TCP加速-一键安装管理脚本.png)

我这里选择“2”，“安装 BBRplus版内核”加速。在安装过程中，可能会出现如下提示，用右方向键选“<No>”，然后回车。如下图所示：

![img](images/TCP加速-一键安装管理脚本-移除原内核警示.png)

安装完成后会提示重启服务器，这时候输入字母“y”，回车后，重启服务器。当服务器启动后，我们再次执行安装命令，选择“7”启用“使用BBRplus版加速”。

## **5、V2Ray客户端配置**

关于V2Ray客户端配置使用教程，请参考文章 [V2Ray客户端下载、安装与配置使用教程 | 支持Windows/Mac/Android/iOS/Linux/路由器全平台](https://iyideng.fun/black-technology/cgfw/v2ray-client-download-and-using-tutorial.html)

## **6、自建vps和买机场哪个好**

如果你有计算机网络技术基础，还懂得那么一点英文的话，一键搭建V2Ray服务器是一件非常简单的事情；否则，对你来说还真是有点麻烦，因为从购买域名和选择VPS、远程登录和执行安装代码等，都是一件很闹心的事情，尤其是有问题的时候，一定会让你崩溃，甚至想放弃。如果你已经决定使用Vultr搭建V2Ray服务器的话，那么搭建时请尽可能使用“Vmess+WS+TLS+Web+CDN”模式实现流量伪装，抗封锁能力强，更加安全稳定，但是速度会有所下降；否则，请选择使用V2Ray机场节点。

## **7、优质V2Ray机场推荐**

不管你是萌新，还是老鸟，使用一键安装脚本搭建V2Ray服务器真的很简单，喜欢折腾的用户可以尝试更多高级的玩法。如果你不喜欢折腾，只想要一个稳定可靠的科学上网工具，那么我推荐你选择一家。有没有比较好的V2Ray订阅节点购买地址？稳定好用的V2Ray机场推荐你使用 [GLaDOS](https://iyideng.fun/special/bgfw/glados-v2ray-wireguard.html)、[FastLink](https://iyideng.fun/special/bgfw/fastlink.html)，GLaDOS是一家平价稳定的Trojan/V2Ray机场，4年老牌机场，采用负载均衡技术，性价比非常高；FastLink是一家高速稳定的SSR/V2Ray机场，使用BGP隧道中转和IPLC内网专线，超过100条优质线路节点，以稳定性强和服务好而广受好评。

**【温馨提醒】**如果您是新手小白，或不能成功搭建科学上网代理服务器，或对线路节点的速度和稳定性均有更高需求，那么[一灯不是和尚](https://iyideng.fun/)推荐您参考文章 [优质高速稳定SS/SSR/Xray/Trojan/V2Ray机场推荐 | 网络加速器梯子推荐](https://iyideng.fun/special/bgfw/best-ss-ssr-v2ray-trojan.html)，它能帮助您挑选一家最适合您的优质SS/SSR/Trojan/Xray/V2Ray机场梯子。

**郑重声明：**本教程仅限于查阅学习资料和从事科研外贸工作的人群，所涉及到的工具资源均来自于互联网，本站对这些资源的可用性、安全性和版权不负有任何责任。如有侵权，请联系我删除。使用过程中，请您务必遵守当地的法律法规。

本文由[一灯不是和尚](https://iyideng.fun/)更新于2023年3月17日；如果您有什么意见或建议，请在文章下面评论区留言反馈。

### 相关文章：

1. [[Shadowsocks/SS搭建教程2023\]使用 Vultr VPS搭建SS服务器及Shadowsocks节点配置客户端实现科学上网](https://iyideng.fun/black-technology/cgfw/shadowsocks-ss-server-building-and-using-tutorial.html)
2. [[ShadowsocksR/SSR搭建教程2023\]使用 Vultr VPS 自建SSR服务器及ShadowsocksR节点配置客户端实现科学上网](https://iyideng.fun/black-technology/cgfw/shadowsocksr-ssr-server-building-and-using-tutorial.html)
3. [[Trojan一键搭建教程2023\]使用 Vultr VPS 自建Trojan/Trojan-Go服务器及客户端配置多用户实现科学上网](https://iyideng.fun/black-technology/cgfw/trojan-server-building-and-using-tutorial.html)
4. [V2Ray的前世今生和工作原理，与SS/SSR的区别以及优质V2Ray机场推荐](https://iyideng.fun/black-technology/cgfw/v2ray-vmess-vless.html)
5. [心阶云/自由鲸 – 高速稳定SSR/V2Ray机场推荐 | CN2/BGP隧道中转/IPLC内网专线](https://iyideng.fun/special/bgfw/xinjiecloud-ssr-v2ray.html)
